
class Log 

  def initialize(logger)
    $logger = logger
  end 

  def self.instance
    $logger ||= Rails.logger
  end

  def self.method_call(instance, method, *args, &block)
    result = nil 
    timestamp = (Time.now.utc.to_f * 1000).to_i
    self.start "ts=#{timestamp} activity=#{instance.class}##{method}",  *args
    time = Benchmark.realtime {result = block.call}
    result
    ensure 
    self.end "ts=#{timestamp} activity=#{instance.class}##{method}", time
  end


  def self.activity(method, other=nil)
    self.debug "activity=#{method.owner}##{method.name}; #{other}"
  end

  def self.start(method, args); instance.debug(%Q[logstate=start #{method} #{args}]) end

  def self.end(method, time_taken=nil)
    time = time_taken ? time_taken.round(3) : nil
    instance.debug("logstate=end #{method} duration=#{time}") 
  end

  def self.error(msg); instance.error(msg) end
  def self.debug(msg); instance.debug(msg) end
  def self.fatal(msg); instance.fatal(msg) end
  def self.info(msg); instance.info(msg) end
  def self.warn(msg); instance.warn(msg) end

end